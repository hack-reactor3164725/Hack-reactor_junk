def calories(entree_num, side_num, dessert_num, drink_num):
#create a dict according to the menu
    menu = {
        'entrees': [522, 399, 501],
        'side': [130, 125, 72],
        'desserts': [222, 291, 100],
        'drinks': [10, 8, 120]
        }

    entree_selection = 0
    side_selection= 0
    dessert_selection = 0
    drink_selection = 0

#store each selection into it's own variable
#create an if statement for each selection to see if the person choose something or passed.
    if entree_num >0:
        entree_selection = menu['entrees'][entree_num - 1]
    if side_num > 0:
        side_selection = menu["side"][side_num - 1]
    if dessert_num > 0:
        dessert_selection = menu['desserts'][dessert_num- 1]
    if drink_num > 0:
        drink_selection = menu["drinks"][drink_num - 1]

    return entree_selection + side_selection + dessert_selection + drink_selection

print(calories(1,1,1,0))
