#import the models
from django.db import models

#create your class
class Book(models.Model):
	title = models.CharFields(max_length=200)
	author = models.CharField(max_length=200)
	year_published = models.SmallIntergerField()
	publisher_name = models.CharField(max_length=100, null=True)
